package com.loccitane.accelerateit.minipim.services

import com.loccitane.accelerateit.minipim.models.ProductFixtures
import com.loccitane.accelerateit.minipim.repositories.SegmentRepository
import io.mockk.every
import io.mockk.mockk
import io.mockk.verify
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test

import org.junit.jupiter.api.BeforeEach

/**
 * @author Alexandre Masselot
 * @Copyright L'Occitane 2021
 */
internal class SegmentServiceTest : ProductFixtures {
    private lateinit var repository: SegmentRepository

    private lateinit var service: SegmentService

    @BeforeEach
    fun setup() {
        repository = mockk()
        service = SegmentService(repository)
    }

    @Test
    fun `list should return a sort list by code`() {
        // given
        val segY = aSegment("y").copy(id = 1L)
        val segX = aSegment("x").copy(id = 2L)

        every { repository.findAll() } returns listOf(segY, segX)

        // when
        val got = service.list()

        // then
        assertThat(got).isEqualTo(listOf(segX, segY))
    }

    @Test
    fun `createOrUpdate should save in DB in case we alter the decription`() {
        // given
        val code = "seg X";
        val givenSegment = aSegment(code)
        val segmentAlreadyInDB = aSegment(code)
            .copy(
                description = "old segment description",
                id = 1L,
            )
        val modifiedSavedSegment = givenSegment.copy(
            id = 1L
        )

        every { repository.findByCode(code) } returns segmentAlreadyInDB
        every { repository.save(givenSegment.copy(id = 1L)) } returns modifiedSavedSegment

        // when
        val got = service.createOrUpdate(givenSegment)

        // then
        assertThat(got).isEqualTo(modifiedSavedSegment)
        verify(exactly = 1) { repository.findByCode(code) }
        // check we have performed no save, as it was already existing
        verify(exactly = 1) { repository.save(any()) }
    }
}
