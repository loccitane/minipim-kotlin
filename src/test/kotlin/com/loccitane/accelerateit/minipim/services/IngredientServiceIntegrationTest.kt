package com.loccitane.accelerateit.minipim.services

import com.loccitane.accelerateit.minipim.MiniPimApplication
import com.loccitane.accelerateit.minipim.models.ProductFixtures
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.transaction.annotation.Transactional

/**
 * In this Integration test, a full context (with in memory database) is spawned
 * We therefore test the global behavior of the system
 * The side effect is that test execution is longer
 *
 * @author Alexandre Masselot
 * @Copyright L'Occitane 2021
 */
@SpringBootTest(classes = [MiniPimApplication::class])
class IngredientServiceIntegrationTest : ProductFixtures {
    @Autowired
    lateinit var ingredientFamilyService: IngredientFamilyService

    @Autowired
    lateinit var ingredientService: IngredientService

    @BeforeEach
    fun setup() {
        ingredientService.createOrUpdate(anIngredient("w", listOf("A", "B")))
        ingredientService.createOrUpdate(anIngredient("x", listOf("A")))
        ingredientService.createOrUpdate(anIngredient("y", listOf("A", "C")))
        ingredientService.createOrUpdate(anIngredient("z", listOf("C")))
    }

    @Test
    @Transactional
    fun `list all ingredient should find 4`() {
        val got = ingredientService.list()

        assertThat(got).hasSize(4)
    }

    @Test
    @Transactional
    fun `list all ingredient should read families`() {
        val got = ingredientService.list().find { it.name == "w" }

        assertThat(got?.families?.map { it.name }).isEqualTo(listOf("A", "B"))
    }

    @Test
    @Transactional
    fun `get element by name should find back the families`() {
        val got = ingredientService.findByName("w")

        assertThat(got?.families?.map { it.name }).isEqualTo(listOf("A", "B"))
    }

    @Test
    @Transactional
    fun `should get the families`() {
        val got = ingredientFamilyService.list()

        assertThat(got).hasSize(3)
    }

    @Test
    @Transactional
    fun `saving twice an ingredient should get the families`() {

        ingredientService.createOrUpdate(anIngredient("w", listOf("A", "B")))

        val got = ingredientService.findByName("w")

        assertThat(got?.families?.map { it.name }).isEqualTo(listOf("A", "B"))
    }


}
