package com.loccitane.accelerateit.minipim.services

import com.loccitane.accelerateit.minipim.models.ProductFixtures
import com.loccitane.accelerateit.minipim.repositories.IngredientFamilyRepository
import io.mockk.every
import io.mockk.mockk
import io.mockk.verify
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test

import org.junit.jupiter.api.BeforeEach

/**
 * @author Alexandre Masselot
 * @Copyright L'Occitane 2021
 */
internal class IngredientFamilyServiceTest : ProductFixtures {
    private lateinit var repository: IngredientFamilyRepository

    private lateinit var service: IngredientFamilyService

    @BeforeEach
    fun setup() {
        repository = mockk()
        service = IngredientFamilyService(repository)
    }

    @Test
    fun list() {
        // given
        val familyZ = anIngredientFamily("Z").copy(id=1L)
        val familyA = anIngredientFamily("A").copy(id=2L)

        every { repository.findAll() } returns listOf(familyZ, familyA)

        // when
        val got = service.list()

        // then
        assertThat(got).isEqualTo(listOf(familyA, familyZ))
    }

    @Test
    fun `createOrUpdate when already exists`() {
        // given
        val name = "famA";
        val family = anIngredientFamily(name)
        val familyWithId = anIngredientFamily(name).copy(id = 1L)

        every { repository.findByName(name) } returns familyWithId

        // when
        val got = service.createOrUpdate(family)

        // then
        assertThat(got).isEqualTo(familyWithId)
        verify(exactly = 1) { repository.findByName(name) }
        verify(exactly = 0) { repository.save(any()) }
    }

    @Test
    fun `createOrUpdate when not existing`() {
        // given
        val name = "famA";
        val family = anIngredientFamily(name)
        val familyWithId = anIngredientFamily(name).copy(id = 1L)

        // ingredient family is not found
        every { repository.findByName(name) } returns null
        every { repository.save(family) } returns familyWithId

        // when
        val got = service.createOrUpdate(family)

        // then
        assertThat(got).isEqualTo(familyWithId)
        verify(exactly = 1) { repository.findByName(name) }
        verify(exactly = 1) { repository.save(family) }
    }
}
