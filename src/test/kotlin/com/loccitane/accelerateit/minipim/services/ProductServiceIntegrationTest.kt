package com.loccitane.accelerateit.minipim.services

import com.loccitane.accelerateit.minipim.CannotFindProductBySkuForPatchingException
import com.loccitane.accelerateit.minipim.MiniPimApplication
import com.loccitane.accelerateit.minipim.model.Ingredient
import com.loccitane.accelerateit.minipim.model.dtos.ProductPatch
import com.loccitane.accelerateit.minipim.model.mappers.IngredientMapper
import com.loccitane.accelerateit.minipim.model.mappers.ProductMapper
import com.loccitane.accelerateit.minipim.models.ProductFixtures
import org.assertj.core.api.Assertions.assertThat
import org.assertj.core.api.Assertions.assertThatThrownBy
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Nested
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.transaction.annotation.Transactional

/**
 * In this Integration test, a full context (with in memory database) is spawned
 * We therefore test the global behavior of the system
 * The side effect is that test execution is longer
 *
 * @author Alexandre Masselot
 * @Copyright L'Occitane 2021
 */
@SpringBootTest(classes = [MiniPimApplication::class])
class ProductServiceIntegrationTest : ProductFixtures {
    @Autowired
    lateinit var ingredientFamilyService: IngredientFamilyService

    @Autowired
    lateinit var ingredientService: IngredientService

    @Autowired
    lateinit var productService: ProductService

    @BeforeEach
    fun setup() {
        ingredientService.createOrUpdate(anIngredient("w", listOf("A", "B")))
        ingredientService.createOrUpdate(anIngredient("x", listOf("A")))
        ingredientService.createOrUpdate(anIngredient("y", listOf("A", "C")))
        ingredientService.createOrUpdate(anIngredient("z", listOf("C")))
    }

    @Test
    @Transactional
    fun `a saved product shall be found`() {
        val sku = "Sku 1"
        productService.createOrUpdate(aCreateProductDTO(sku = sku))

        val product = productService.findBySku(sku)

        assertThat(product).isNotNull()
        assertThat(product?.modifiedAt).isNotNull()
    }

    @Test
    @Transactional
    fun `saving a product with unknown ingredient shall create it`() {
        val sku = "Sku 1"

        val gotIngredient = ingredientService.existsByName("duh")
        assertThat(gotIngredient).isEqualTo(false)

        productService.createOrUpdate(
            aCreateProductDTO(
                sku = sku,
                ingredients = listOf("duh")
            )
        )
        val gotIngredient2 = ingredientService.existsByName("duh")
        assertThat(gotIngredient2).isEqualTo(true)
    }


    @Test
    @Transactional
    fun `changing a product`() {
        val sku = "sku"
        val dto1 = aCreateProductDTO(
            sku = sku,
            replaced_by = "rep 1",
            description = "description 1",
            segmentCode = "S01",
            mad = "2020-01-17",
            ingredients = listOf("paf", "the")
        )
        val dto2 = aCreateProductDTO(
            sku = sku,
            replaced_by = "rep 2",
            description = "description 2",
            segmentCode = "S02",
            mad = "2020-02-17",
            ingredients = listOf("the", "xog")
        )

        productService.createOrUpdate(dto1)
        val got1 = productService.findBySku(sku)
        assertThat(got1?.replacedBy).isEqualTo(dto1.replaced_by)
        assertThat(got1?.description).isEqualTo(dto1.description)
        assertThat(got1?.segment?.code).isEqualTo(dto1.segment.code)
        assertThat(got1?.mad).isEqualTo(dto1.mad)
        assertThat(got1?.ingredients?.map { it.name }).isEqualTo(dto1.ingredients)

        productService.createOrUpdate(dto2)
        val got2 = productService.findBySku(sku)
        assertThat(got2?.replacedBy).isEqualTo(dto2.replaced_by)
        assertThat(got2?.description).isEqualTo(dto2.description)
        assertThat(got2?.segment?.code).isEqualTo(dto2.segment.code)
        assertThat(got2?.mad).isEqualTo(dto2.mad)
        assertThat(got2?.ingredients?.map { it.name }?.sorted()).isEqualTo(dto2.ingredients)
    }

    @Test
    @Transactional
    fun `a not saved product shall be found`() {
        val sku = "Sku 1"
        val product = productService.findBySku(sku)
        assertThat(product).isNull()
    }

    @Test
    @Transactional
    fun `creating three product but two with the same SKU should give a list of 2`() {
        productService.createOrUpdate(aCreateProductDTO(sku = "Sku 1"))
        productService.createOrUpdate(aCreateProductDTO(sku = "Sku 2"))
        productService.createOrUpdate(aCreateProductDTO(sku = "Sku 1"))

        val got = productService.list()

        assertThat(got).hasSize(2)
    }

    @Test
    @Transactional
    fun `patching alters the product description`() {
        val sku = "Sku 1"
        productService.createOrUpdate(aCreateProductDTO(sku = sku))
        val patch = ProductPatch(description = "another description")

        productService.patch(sku, patch)
        val product = productService.findBySku(sku)

        assertThat(product?.description).isEqualTo("another description")
    }

    @Test
    @Transactional
    fun `patching a non existing product shall throw an exception`() {
        val sku = "Sku 1"
        val patch = ProductPatch(description = "another description")

        assertThatThrownBy { productService.patch(sku, patch) }.isInstanceOf(
            CannotFindProductBySkuForPatchingException::class.java
        )
    }


    @Test
    @Transactional
    fun `a deleted product shall not be found back`() {
        val sku = "Sku 1"
        productService.createOrUpdate(aCreateProductDTO(sku = sku))

        productService.delete(sku)
        val product = productService.findBySku(sku)

        assertThat(product).isNull()
    }

    @Test
    @Transactional
    fun `delete a none existing product shall throw an exception`() {
        val sku = "Sku 1"
        assertThatThrownBy { productService.delete(sku) }.isInstanceOf(
            CannotFindProductBySkuForDeletingException::class.java
        )
    }

    @Nested
    open inner class FindAllByFamily {
        @BeforeEach
        fun setup() {
            ingredientService.createOrUpdate(anIngredient("w", listOf("A", "B")))
            ingredientService.createOrUpdate(anIngredient("x", listOf("A")))
            ingredientService.createOrUpdate(anIngredient("y", listOf("A", "C")))
            ingredientService.createOrUpdate(anIngredient("z", listOf("C")))

            productService.createOrUpdate(aCreateProductDTO(sku = "Pwxyz", ingredients = listOf("w", "x", "y", "z")))
            productService.createOrUpdate(aCreateProductDTO(sku = "Pxyz", ingredients = listOf("x", "y", "z")))
            productService.createOrUpdate(aCreateProductDTO(sku = "Pw", ingredients = listOf("w")))
        }

        @Test
        @Transactional
        open fun `should find three products with A`() {
            println(ingredientService.list())
            val got = productService.findAllByFamily("A")

            assertThat(got.map { it.sku }.sortedBy { it }).isEqualTo(listOf("Pw", "Pwxyz", "Pxyz"))
        }

        @Test
        @Transactional
        open fun `should find two products with B`() {
            val got = productService.findAllByFamily("B")

            assertThat(got.map { it.sku }.sortedBy { it }).isEqualTo(listOf("Pw", "Pwxyz"))
        }

        @Test
        @Transactional
        open fun `should find none with unknown family`() {
            val got = productService.findAllByFamily("PAF")

            assertThat(got).hasSize(0)
        }
    }
}
