package com.loccitane.accelerateit.minipim.services

import com.loccitane.accelerateit.minipim.model.Ingredient
import com.loccitane.accelerateit.minipim.models.ProductFixtures
import com.loccitane.accelerateit.minipim.repositories.IngredientRepository
import io.mockk.every
import io.mockk.mockk
import io.mockk.verify
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test

/**
 * This IngredientServiceTest is purely Unit test (no dependencies).
 * It means that all interactions must be mocked
 * And a deep knowledge of the underlying layer is needed
 *
 * @author Alexandre Masselot
 * @Copyright L'Occitane 2021
 */
internal class IngredientServiceTest : ProductFixtures {
    private lateinit var repository: IngredientRepository
    private lateinit var ingredientFamilyService: IngredientFamilyService

    private lateinit var service: IngredientService

    @BeforeEach
    fun setup() {
        repository = mockk()
        ingredientFamilyService = mockk()
        service = IngredientService(repository, ingredientFamilyService)
    }

    @Test
    fun `createOrUpdate where one familyExists`() {
        // given
        val familyNameA = "fam A";
        val familyA = anIngredientFamily(familyNameA)
        val familyAWithId = anIngredientFamily(familyNameA).copy(id = 1L)

        val familyNameB = "fam B";
        val familyB = anIngredientFamily(familyNameB)
        val familyBWithId = anIngredientFamily(familyNameB).copy(id = 2L)

        val ingredientName = "ing X"
        // ingredient with no ids at all (the one passed for saving)
        val ingredient = Ingredient(
            name = ingredientName,
            families = mutableSetOf(familyA, familyB)
        )
        // ingredient, where family have id (th one internally passed to repository save)
        val ingredientWithFamilyId = ingredient.copy(
            families = mutableSetOf(familyAWithId, familyBWithId),
        )
        // ingredient with full id, the one saved in the DB
        val ingredientWithId = ingredientWithFamilyId.copy(
            id = 3L
        )

        every { repository.findByName(ingredientName) } returns null
        every { ingredientFamilyService.createOrUpdate(familyA) } returns familyAWithId
        every { ingredientFamilyService.createOrUpdate(familyB) } returns familyBWithId
        every { repository.save(ingredientWithFamilyId) } returns ingredientWithId

        // when
        val got = service.createOrUpdate(ingredient)

        // then
        assertThat(got).isEqualTo(ingredientWithId)
        // ingredient was search once iwth the name
        verify(exactly = 1) { repository.findByName(ingredientName) }
        // ingredient was saved in DB
        verify(exactly = 1) { repository.save(any()) }
        // ingredient family was called twice for create or update
        verify(exactly = 2) { ingredientFamilyService.createOrUpdate(any()) }

    }
}
