package com.loccitane.accelerateit.minipim.model

import com.loccitane.accelerateit.minipim.models.ProductFixtures
import org.assertj.core.api.Assertions
import org.junit.jupiter.api.Test

/**
 * @author Alexandre Masselot
 * @Copyright L'Occitane 2021
 */
internal class ProductTest: ProductFixtures {

    @Test
    fun contains() {
        val product = aProduct()

        val got = product.formulaContains()

        Assertions.assertThat(got.map{it.name}.sorted()).isEqualTo(listOf("A", "B", "C"))
    }
}
