package com.loccitane.accelerateit.minipim.models.mappers

import com.loccitane.accelerateit.minipim.model.Ingredient
import com.loccitane.accelerateit.minipim.model.ProductType
import com.loccitane.accelerateit.minipim.model.dtos.ProductSummaryDTO
import com.loccitane.accelerateit.minipim.model.mappers.ProductMapper
import com.loccitane.accelerateit.minipim.models.ProductFixtures
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test

/**
@author Alexandre Masselot
@Copyright L'Occitane 2021
 */
class ProductMapperTest : ProductFixtures {
    private val product = aProduct()
    private val createProductDTO = aCreateProductDTO()
    private val productDTO = aProductDTO()

    @Test
    fun fromDto() {

        // When
        val got = ProductMapper.fromDto(createProductDTO)

        // Then
        // must tweak
        //  *the modified_time, inferred from now()
        // remove families from ingredients, as DTO does not know them
        assertThat(got).isEqualTo(
            product.copy(
                modifiedAt = got.modifiedAt,
                ingredients = product.ingredients.map { it.copy(families = mutableSetOf()) }.toMutableSet()
            )
        )
    }

    @Test
    fun toDto() {
        // When
        val got = ProductMapper.toDto(product)

        // Then
        assertThat(got).isEqualTo(productDTO)
    }

    @Test
    fun toSummaryDto() {
        // When
        val got = ProductMapper.toSummaryDto(product)

        // Then
        assertThat(got).isEqualTo(ProductSummaryDTO(product.getProductType(), product.sku, product.description, null))
    }
}
