package com.loccitane.accelerateit.minipim.models.mappers

import com.loccitane.accelerateit.minipim.model.mappers.IngredientMapper
import com.loccitane.accelerateit.minipim.models.ProductFixtures
import org.junit.jupiter.api.Test

import org.assertj.core.api.Assertions.assertThat


/**
@author Alexandre Masselot
@Copyright L'Occitane 2021
 */
class IngredientMapperTest : ProductFixtures {
    private val ingredientDto = anIngredientDTO("x", listOf("A", "B"))
    private val ingredient = anIngredient("x", listOf("A", "B"))

    @Test
    fun fromDto() {

        // When
        val gotIngredient = IngredientMapper.fromDto(ingredientDto)

        // Then
        assertThat(gotIngredient).isEqualTo(ingredient)
    }

    @Test
    fun toDto() {
        // When
        val got = IngredientMapper.toDto(ingredient)

        // Then
        assertThat(got).isEqualTo(ingredientDto)
    }
}
