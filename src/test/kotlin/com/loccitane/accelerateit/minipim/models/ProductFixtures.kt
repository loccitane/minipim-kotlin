package com.loccitane.accelerateit.minipim.models

import com.loccitane.accelerateit.minipim.model.*
import com.loccitane.accelerateit.minipim.model.dtos.*
import java.sql.Date
import java.sql.Timestamp
import java.time.Instant

/**
 * Fixtures are the creation of models, for the sake of testing
@author Alexandre Masselot
@Copyright L'Occitane 2021
 */
interface ProductFixtures {
    fun aProduct(
        sku: String = "P1",
        oldSku: String = "P99",
        description: String = "test product",
        segmentCode: String = "S01",
        mad: String = "2020-03-20",
        ingredientAndFamilies: List<Pair<String, List<String>>> = listOf(
            "w" to listOf("A", "B"),
            "x" to listOf("A"),
            "y" to listOf("A", "C"),
            "z" to listOf("C")
        ),
        logistics: Logistics = aLogistics(
            netWeight = 42.0,
            grossWeight = 63.0,
            height = 3.0,
            width = 5.0,
            length = 7.0
        ),
        modifiedAt: String = "1931-01-15T10:15:30Z"
    ) = Product(
        sku = sku,
        replacedBy = oldSku,
        description = description,
        segment = aSegment(segmentCode),
        mad = Date.valueOf(mad),
        ingredients = anIngredientSet(ingredientAndFamilies),
        logistics = logistics,
        modifiedAt = Timestamp.from(Instant.parse(modifiedAt))
    )

    fun aProductDTO(
        sku: String = "P1",
        type: ProductType = ProductType.PRODUCT,
        oldSku: String = "P99",
        description: String = "test product",
        segmentCode: String = "S01",
        mad: String = "2020-03-20",
        ingredients: List<String> = listOf(
            "w",
            "x",
            "y",
            "z",
        ),
        formula_contains: List<String> = listOf("A", "B", "C"),
        logistics: LogisticsDTO = aLogisticsDTO(
            netWeight = 42.0,
            grossWeight = 63.0,
            height = 3.0,
            width = 5.0,
            length = 7.0
        ),
        modifiedAt: String = "1931-01-15T10:15:30Z"
    ) =
        ProductDTO(
            type = type,
            sku = sku,
            replaced_by = oldSku,
            description = description,
            segment = aSegmentDTO(segmentCode),
            mad = Date.valueOf(mad),
            ingredients = ingredients,
            formula_contains = formula_contains,
            logistics = logistics,
            product_quantities = null,
            modified_at = Instant.parse(modifiedAt)
        )

    fun aCreateProductDTO(
        sku: String = "P1",
        replaced_by: String = "P99",
        description: String = "test product",
        segmentCode: String = "S01",
        mad: String = "2020-03-20",
        ingredients: List<String> = listOf(
            "w",
            "x",
            "y",
            "z",
        ),
        logistics: LogisticsDTO = aLogisticsDTO(
            netWeight = 42.0,
            grossWeight = 63.0,
            height = 3.0,
            width = 5.0,
            length = 7.0
        )
    ) =
        ProductCreateUpdateDTO(
            sku = sku,
            replaced_by = replaced_by,
            description = description,
            segment = aSegmentDTO(segmentCode),
            mad = Date.valueOf(mad),
            ingredients = ingredients,
            logistics = logistics,
            product_quantities = null
        )

    fun aSegment(code: String = "S01") = Segment(code, "test segment ($code)")
    fun aSegmentDTO(code: String = "S01") = SegmentDTO(code, "test segment ($code)")

    /**
     * create a list of ingredients
     * @param ingToFamilies = listOf(
     *            "w" to listOf("A", "B"),
     *            "x" to listOf("A"),
     *            "y" to listOf("A", "C"),
     *            "z" to listOf("C")
     *            ))
     */
    fun anIngredientSet(ingToFamilies: List<Pair<String, List<String>>>): MutableSet<Ingredient> =
        ingToFamilies.map { anIngredient(it.first, it.second) }.toMutableSet()

    fun anIngredient(ingredientName: String, familyNames: List<String>) =
        Ingredient(ingredientName, anIngredientFamilySet(familyNames))

    fun anIngredientDTO(ingredientName: String, familyNames: List<String>) =
        IngredientDTO(ingredientName, familyNames)

    /**
     * From a list of IngredientFamily names, build a set of IngredientFamily
     */
    fun anIngredientFamilySet(names: List<String>): MutableSet<IngredientFamily> =
        names.map { name -> IngredientFamily(name) }.toMutableSet()

    /**
     * From a list of IngredientFamily names, build a set of IngredientFamily
     */
    fun anIngredientFamily(name: String): IngredientFamily =
        IngredientFamily(name)

    fun aLogistics(
        netWeight: Double = 42.0,
        grossWeight: Double = 63.0,
        height: Double = 3.0,
        width: Double = 5.0,
        length: Double = 7.0
    ): Logistics =
        Logistics(
            netWeight = netWeight,
            grossWeight = grossWeight,
            height = height,
            width = width,
            length = length
        )

    fun aLogisticsDTO(
        netWeight: Double = 42.0,
        grossWeight: Double = 63.0,
        height: Double = 3.0,
        width: Double = 5.0,
        length: Double = 7.0
    ) =
        LogisticsDTO(
            netWeight = netWeight,
            grossWeight = grossWeight,
            height = height,
            width = width,
            length = length
        )
}
