create sequence hibernate_sequence start with 1 increment by 1
create table ingredient (id bigint not null, name varchar(255), primary key (id))
create table ingredient_families (ingredient_id bigint not null, families_id bigint not null, primary key (ingredient_id, families_id))
create table ingredient_family (id bigint not null, name varchar(255), primary key (id))
create table kit_to_product_relationship (id bigint not null, quantity integer, product_id bigint, primary key (id))
create table logistics (id bigint not null, gross_weight double, height double, length double, net_weight double, width double, primary key (id))
create table product (id bigint not null, description varchar(255), mad date, modified_at timestamp, replaced_by varchar(255), sku varchar(255), logistics_id bigint, segment_id bigint, primary key (id))
create table product_ingredients (product_id bigint not null, ingredients_id bigint not null, primary key (product_id, ingredients_id))
create table product_product_quantities (product_id bigint not null, product_quantities_id bigint not null, primary key (product_id, product_quantities_id))
create table segment (id bigint not null, code varchar(255), description varchar(255), primary key (id))
alter table ingredient add constraint UK_bcuaj97y3iu3t2vj26jg6hijj unique (name)
alter table ingredient_family add constraint UK_sx74em050i5h56a25yy7a35h4 unique (name)
alter table product add constraint UK_q1mafxn973ldq80m1irp3mpvq unique (sku)
alter table product_product_quantities add constraint UK_8dllvtj6rwgmrjjmh6qfsp3h3 unique (product_quantities_id)
alter table segment add constraint UK_guaq9prmvyn2obgvgilc0n0km unique (code)
alter table segment add constraint UK_ol9w1mvpp8tf69v1jejajrpum unique (description)
alter table ingredient_families add constraint FK236ip9gym4o7p709j1hv2vxn3 foreign key (families_id) references ingredient_family
alter table ingredient_families add constraint FKshlxb3k0u3817b6t79cjsmwei foreign key (ingredient_id) references ingredient
alter table kit_to_product_relationship add constraint FKsfbqfu28qtyxvxp6h2349nyn7 foreign key (product_id) references product
alter table product add constraint FKdehi7cyoh0p0m8ji13ye8wcry foreign key (logistics_id) references logistics
alter table product add constraint FK4iitw8wsvr5mc1990fs1061b3 foreign key (segment_id) references segment
alter table product_ingredients add constraint FKq51ay36hx4j4y345v8bkr2dix foreign key (ingredients_id) references ingredient
alter table product_ingredients add constraint FKcjxxp1v84pkvofdwi58x85431 foreign key (product_id) references product
alter table product_product_quantities add constraint FKbr9br2qi5bl9we3sctqtea7da foreign key (product_quantities_id) references kit_to_product_relationship
alter table product_product_quantities add constraint FKthjdib9evbpbdon60ywf07c4x foreign key (product_id) references product
