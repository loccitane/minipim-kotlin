package com.loccitane.accelerateit.minipim

import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.ResponseStatus

/**
@author Alexandre Masselot
@Copyright L'Occitane 2021
 */
@ResponseStatus(value = HttpStatus.NOT_FOUND)
class CannotFindIngredientByNameException(name: String) :
    RuntimeException("Cannot find ingredient with name=[$name]")

@ResponseStatus(value = HttpStatus.NOT_FOUND)
class CannotFindProductBySkuException(sku: String) :
    RuntimeException("Cannot find product with sku=[$sku]")

@ResponseStatus(value = HttpStatus.NOT_FOUND)
class CannotFindProductBySkuForPatchingException(sku: String) :
    RuntimeException("Cannot find product with sku=[$sku] for patching")
