package com.loccitane.accelerateit.minipim.controllers

import com.loccitane.accelerateit.minipim.CannotFindProductBySkuException
import com.loccitane.accelerateit.minipim.model.dtos.ProductCreateUpdateDTO
import com.loccitane.accelerateit.minipim.model.dtos.ProductDTO
import com.loccitane.accelerateit.minipim.model.dtos.ProductPatch
import com.loccitane.accelerateit.minipim.model.dtos.ProductSummaryDTO
import com.loccitane.accelerateit.minipim.model.mappers.ProductMapper
import com.loccitane.accelerateit.minipim.services.CannotDeleteProductBecauseOfDependencies
import com.loccitane.accelerateit.minipim.services.ProductService
import org.springframework.dao.DataIntegrityViolationException
import org.springframework.web.bind.annotation.*

/**
@author Alexandre Masselot
@Copyright L'Occitane 2021
 */
@RestController
@RequestMapping("/products")
class ProductController(
    val service: ProductService
) {
    @GetMapping("")
    fun list(
        @RequestParam(name = "withFormulaContains", required = false) withFormulaContains: String? = null
    ): List<ProductSummaryDTO> {
        val products = if (withFormulaContains == null) {
            service.list()
        } else {
            service.findAllByFamily(withFormulaContains)
        }
        return products.map { ProductMapper.toSummaryDto(it) }
    }


    @GetMapping("/{sku}")
    fun findBySku(
        @PathVariable sku: String
    ): ProductDTO {
        val product = service.findBySku(sku) ?: throw CannotFindProductBySkuException(sku)
        return ProductMapper.toDto(product)
    }

    @PostMapping("")
    fun createOrUpdate(
        @RequestBody productDTO: ProductCreateUpdateDTO
    ): ProductDTO {
        val product = service.createOrUpdate(productDTO)
        return ProductMapper.toDto(product)
    }

    @PatchMapping("/{sku}")
    fun patch(
        @PathVariable sku: String,
        @RequestBody productPatch: ProductPatch
    ) {
        service.patch(sku, productPatch)
    }

    @DeleteMapping("/{sku}")
    fun delete(
        @PathVariable sku: String
    ) {
        try {
            service.delete(sku)
        } catch (e: DataIntegrityViolationException) {
            throw CannotDeleteProductBecauseOfDependencies(sku)
        }
    }

}
