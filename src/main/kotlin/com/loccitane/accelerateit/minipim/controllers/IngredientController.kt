package com.loccitane.accelerateit.minipim.controllers

import com.loccitane.accelerateit.minipim.CannotFindIngredientByNameException
import com.loccitane.accelerateit.minipim.model.dtos.IngredientDTO
import com.loccitane.accelerateit.minipim.model.mappers.IngredientMapper
import com.loccitane.accelerateit.minipim.services.IngredientService
import org.springframework.web.bind.annotation.*

/**
@author Alexandre Masselot
@Copyright L'Occitane 2021
 */
@RestController
@RequestMapping("/ingredients")
class IngredientController(
    val service: IngredientService
) {
    @GetMapping("")
    fun list(): List<IngredientDTO> =
        service.list().map { IngredientMapper.toDto(it) }


    @GetMapping("/{name}")
    fun findByName(
        @PathVariable name: String
    ): IngredientDTO {
        val ingredient = service.findByName(name) ?: throw CannotFindIngredientByNameException(name)
        return IngredientMapper.toDto(ingredient)
    }

    @PostMapping("")
    fun createOrUpdate(
        @RequestBody
        dto: IngredientDTO
    ): IngredientDTO {
        val ingredient = service.createOrUpdate(IngredientMapper.fromDto(dto))
        return IngredientMapper.toDto(ingredient)
    }

    @DeleteMapping("/{name}")
    fun delete(
        @PathVariable(name = "name", required = true) name: String
    ) {
            service.delete(name)
    }

}
