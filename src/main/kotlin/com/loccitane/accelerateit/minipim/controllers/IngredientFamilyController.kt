package com.loccitane.accelerateit.minipim.controllers

import com.loccitane.accelerateit.minipim.services.IngredientFamilyService
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

/**
@author Alexandre Masselot
@Copyright L'Occitane 2021
 */
@RestController
@RequestMapping("/ingredient-families")
class IngredientFamilyController(
    val service: IngredientFamilyService
) {
    @GetMapping("")
    fun list(): List<String> =
        service.list().map { it.name }
}
