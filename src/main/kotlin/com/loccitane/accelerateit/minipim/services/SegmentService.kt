package com.loccitane.accelerateit.minipim.services

import com.loccitane.accelerateit.minipim.model.Segment
import com.loccitane.accelerateit.minipim.repositories.SegmentRepository
import org.springframework.stereotype.Service

/**
@author Alexandre Masselot
@Copyright L'Occitane 2021
 */
@Service
class SegmentService(
    val repository: SegmentRepository
) {
    fun list() =
        repository.findAll().toList().sortedBy { it.code }

    /**
     * create or update a segment. To ensure uniqueness on code, with find by code first
     */
    fun createOrUpdate(segment: Segment): Segment {
        val inDb = repository.findByCode(segment.code)
        return repository.save(segment.copy(id = inDb?.id))
    }
}
