package com.loccitane.accelerateit.minipim.services

import com.loccitane.accelerateit.minipim.model.Ingredient
import com.loccitane.accelerateit.minipim.repositories.IngredientRepository
import org.springframework.dao.DataIntegrityViolationException
import org.springframework.stereotype.Service

/**
@author Alexandre Masselot
@Copyright L'Occitane 2021
 */
@Service
class IngredientService(
    val repository: IngredientRepository,
    val ingredientFamilyService: IngredientFamilyService
) {
    fun list(): List<Ingredient> =
        repository.findAll().toList()

    fun findByName(name: String): Ingredient? =
        repository.findByName(name)

    fun existsByName(name: String): Boolean =
        repository.existsByName(name)

    /**
     * create an ingredient or update it if it already exists.
     * The provided IngredientFamily elements will also be created or updated
     */
    fun createOrUpdate(ingredient: Ingredient): Ingredient {
        // save families or get them from DB
        val families = ingredient.families.map { family -> ingredientFamilyService.createOrUpdate(family) }
        // get the ingredient from the db (by unique name) if it exist
        val ingredientInDb = repository.findByName(ingredient.name) ?: ingredient;

        ingredientInDb.families.clear()
        ingredientInDb.families.addAll(families)

        // persist in DB
        return repository.save(ingredientInDb)
    }

    /**
     * delete ingredient name, except if it has some dependency exception
     */
    fun delete(name: String) {
        try {
            repository.findByName(name)?.let { repository.delete(it) }
        } catch (e: DataIntegrityViolationException) {
            throw CannotDeleteIngredientBecauseOfDependencies(name)
        }
    }
}
