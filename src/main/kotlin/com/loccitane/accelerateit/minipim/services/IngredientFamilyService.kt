package com.loccitane.accelerateit.minipim.services

import com.loccitane.accelerateit.minipim.model.Ingredient
import com.loccitane.accelerateit.minipim.model.IngredientFamily
import com.loccitane.accelerateit.minipim.model.mappers.IngredientMapper
import com.loccitane.accelerateit.minipim.repositories.IngredientFamilyRepository
import com.loccitane.accelerateit.minipim.repositories.IngredientRepository
import org.springframework.stereotype.Service

/**
@author Alexandre Masselot
@Copyright L'Occitane 2021
 */
@Service
class IngredientFamilyService(
    val repository: IngredientFamilyRepository
) {
    fun list() =
        repository.findAll().toList().sortedBy { it.name }

    fun createOrUpdate(ingredientFamily:IngredientFamily): IngredientFamily{
        val inDb = repository.findByName(ingredientFamily.name)
        return inDb?:repository.save(ingredientFamily)
    }
}
