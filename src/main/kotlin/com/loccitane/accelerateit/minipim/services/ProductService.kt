package com.loccitane.accelerateit.minipim.services

import com.loccitane.accelerateit.minipim.CannotFindProductBySkuForPatchingException
import com.loccitane.accelerateit.minipim.model.KitToProductRelationship
import com.loccitane.accelerateit.minipim.model.Product
import com.loccitane.accelerateit.minipim.model.dtos.ProductCreateUpdateDTO
import com.loccitane.accelerateit.minipim.model.dtos.ProductPatch
import com.loccitane.accelerateit.minipim.model.mappers.ProductMapper
import com.loccitane.accelerateit.minipim.repositories.ProductRepository
import org.springframework.dao.DataIntegrityViolationException
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional

/**
@author Alexandre Masselot
@Copyright L'Occitane 2021
 */
@Service
class ProductService(
    val repository: ProductRepository,
    val segmentService: SegmentService,
    val ingredientService: IngredientService
) {
    fun list() =
        repository.findAll().toList().sortedBy { it.sku }

    fun findAllByFamily(family: String) =
        repository.findAllByIngredientsFamiliesNameContains(family).toList().distinct().sortedBy { it.sku }

    fun findBySku(sku: String) = repository.findBySku(sku)

    /**
     * Create or update a product and all the members
     */
    fun createOrUpdate(productDTO: ProductCreateUpdateDTO): Product {
        val product = ProductMapper.fromDto(productDTO)

        // check if some version exists by sku
        val productDB = repository.findBySku(product.sku) ?: product

        // get the ingredients from the DB
        val ingredientsDB = product.ingredients
            .map {
                ingredientService.findByName(it.name) ?: ingredientService.createOrUpdate(it)
            }
            .toMutableSet()

        // save the properties
        val segmentDB = segmentService.createOrUpdate(product.segment)

        productDB.replacedBy = product.replacedBy
        productDB.description = product.description
        productDB.segment = segmentDB
        productDB.mad = product.mad
        productDB.ingredients.clear()
        productDB.ingredients.addAll(ingredientsDB)

        val savedProduct = repository.save(productDB)
        if (productDTO.product_quantities == null) {
            return savedProduct
        }
        val kitRelations = createOrUpdateKitRelationships(
            savedProduct.sku,
            savedProduct.id!!,
            productDTO.product_quantities
        )
        savedProduct.productQuantities.clear()
        savedProduct.productQuantities.addAll(kitRelations)

        return repository.save(savedProduct)

    }

    fun createOrUpdateKitRelationships(
        productSku: String,
        productId: Long,
        relationDtos: Map<String, Int>
    ): List<KitToProductRelationship> {
        val targetProducts = relationDtos.keys
            .map { sku ->
                sku to (findBySku(sku) ?: throw CannotFindProductForKitException(sku, productSku))
            }
            .toMap()

        return relationDtos.entries.map { (productSku, quantity) ->
            KitToProductRelationship(targetProducts[productSku]!!, quantity)
        }
    }

    /**
     *
     */
    fun patch(sku: String, patch: ProductPatch) {
        val product = findBySku(sku) ?: throw CannotFindProductBySkuForPatchingException(sku)
        if (patch.description != null) {
            product.description = patch.description
        }
        repository.save(product)
    }

    @Transactional
    fun delete(sku: String) {
        if (!repository.existsBySku(sku)) {
            throw CannotFindProductBySkuForDeletingException(sku)
        }
        repository.deleteBySku(sku)
    }
}
