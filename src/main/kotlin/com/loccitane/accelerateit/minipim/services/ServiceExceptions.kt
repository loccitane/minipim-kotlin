package com.loccitane.accelerateit.minipim.services

import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.ResponseStatus

/**
@author Alexandre Masselot
@Copyright L'Occitane 2021
 */
@ResponseStatus(HttpStatus.FAILED_DEPENDENCY)
class CannotDeleteIngredientBecauseOfDependencies(name: String) :
    RuntimeException("Cannot delete ingredient because it has some dependencies name=[$name]")

@ResponseStatus(HttpStatus.FAILED_DEPENDENCY)
class CannotDeleteProductBecauseOfDependencies(sku: String) :
    RuntimeException("Cannot delete product because it has some dependencies name=[$sku]")

@ResponseStatus(HttpStatus.FAILED_DEPENDENCY)
class CannotFindIngredientForProductException(ingredientName: String, skuProduct: String) :
    RuntimeException("Cannot find ingredient name=[$ingredientName] for product sku=[$skuProduct] ")

@ResponseStatus(HttpStatus.FAILED_DEPENDENCY)
class CannotFindProductForKitException(skuProduct: String, skuKit: String) :
    RuntimeException("Cannot find product sku=[$skuProduct] for kit=[$skuKit]")

@ResponseStatus(value = HttpStatus.NOT_FOUND)
class CannotFindProductBySkuForDeletingException(sku: String) :
    RuntimeException("Cannot find product with sku=[$sku] for deleting")
