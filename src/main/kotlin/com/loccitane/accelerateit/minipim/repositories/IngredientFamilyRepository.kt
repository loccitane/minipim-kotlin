package com.loccitane.accelerateit.minipim.repositories

import com.loccitane.accelerateit.minipim.model.IngredientFamily
import org.springframework.data.repository.CrudRepository
import org.springframework.stereotype.Repository

/**
@author Alexandre Masselot
@Copyright L'Occitane 2021
 */
@Repository
interface IngredientFamilyRepository:CrudRepository<IngredientFamily, Long> {
    fun findByName(name:String):IngredientFamily?
}
