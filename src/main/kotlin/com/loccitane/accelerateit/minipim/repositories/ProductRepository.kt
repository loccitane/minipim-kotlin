package com.loccitane.accelerateit.minipim.repositories

import com.loccitane.accelerateit.minipim.model.Product
import org.springframework.data.repository.CrudRepository
import org.springframework.stereotype.Repository

/**
@author Alexandre Masselot
@Copyright L'Occitane 2021
 */
@Repository
interface ProductRepository : CrudRepository<Product, Long> {
    fun findBySku(sku: String): Product?
    fun findAllByIngredientsFamiliesNameContains(family: String): List<Product>
    fun existsBySku(sku: String): Boolean
    fun deleteBySku(sku: String): Unit
}
