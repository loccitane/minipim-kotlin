package com.loccitane.accelerateit.minipim.repositories

import com.loccitane.accelerateit.minipim.model.Segment
import org.springframework.data.repository.CrudRepository
import org.springframework.stereotype.Repository

/**
@author Alexandre Masselot
@Copyright L'Occitane 2021
 */
@Repository
interface SegmentRepository:CrudRepository<Segment, Long> {
    fun findByCode(code:String):Segment?
}
