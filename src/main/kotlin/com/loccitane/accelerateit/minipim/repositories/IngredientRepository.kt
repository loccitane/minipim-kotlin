package com.loccitane.accelerateit.minipim.repositories

import com.loccitane.accelerateit.minipim.model.Ingredient
import org.springframework.data.repository.CrudRepository
import org.springframework.stereotype.Repository

/**
@author Alexandre Masselot
@Copyright L'Occitane 2021
 */
@Repository
interface IngredientRepository : CrudRepository<Ingredient, Long> {
    fun existsByName(name: String): Boolean
    fun findByName(name: String): Ingredient?
    fun deleteByName(name: String): Unit
}
