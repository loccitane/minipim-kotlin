package com.loccitane.accelerateit.minipim.model.dtos

import com.fasterxml.jackson.annotation.JsonInclude
import com.loccitane.accelerateit.minipim.model.ProductType
import java.sql.Date
import java.time.Instant

/**
@author Alexandre Masselot
@Copyright L'Occitane 2021
 */

@JsonInclude(JsonInclude.Include.NON_NULL)
data class ProductDTO(
    val type: ProductType,
    val sku: String,
    val replaced_by: String?,
    val description: String,
    val mad: Date,
    val segment: SegmentDTO,
    val ingredients: List<String>,
    val formula_contains: List<String>,
    val logistics: LogisticsDTO,
    val product_quantities: List<KitToProductRelationshipDTO>?,
    val modified_at: Instant,
)
