package com.loccitane.accelerateit.minipim.model

import org.hibernate.annotations.UpdateTimestamp
import java.sql.Date
import java.sql.Timestamp
import javax.persistence.*

enum class ProductType {
    PRODUCT,
    KIT;
}

/**
@author Alexandre Masselot
@Copyright L'Occitane 2021
 */
@Entity
data class Product(
    @Column(unique = true)
    var sku: String,
    @Column
    var replacedBy: String?,
    @Column
    var description: String,
    @ManyToOne(fetch = FetchType.LAZY)
    var segment: Segment,
    @Column
    var mad: Date,
    @ManyToMany(fetch = FetchType.LAZY)
    var ingredients: MutableSet<Ingredient> = mutableSetOf(),
    @OneToOne(cascade = [CascadeType.ALL], fetch = FetchType.LAZY)
    var logistics: Logistics,
    @OneToMany(cascade = [CascadeType.ALL], fetch = FetchType.LAZY)
    var productQuantities: MutableSet<KitToProductRelationship> = mutableSetOf(),
    @Column @UpdateTimestamp
    var modifiedAt: Timestamp,
    @Id @GeneratedValue
    var id: Long? = null
) {
    fun formulaContains(): List<IngredientFamily> = ingredients.flatMap { it.families }.distinct()

    fun getProductType() = if (productQuantities.isEmpty()) ProductType.PRODUCT else ProductType.KIT
}
