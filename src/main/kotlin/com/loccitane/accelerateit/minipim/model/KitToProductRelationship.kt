package com.loccitane.accelerateit.minipim.model

import javax.persistence.*

/**
@author Alexandre Masselot
@Copyright L'Occitane 2021
 */
@Entity
data class KitToProductRelationship (
    @ManyToOne
    var product: Product,
    @Column
    var quantity: Int,
    @Id
    @GeneratedValue
    var id: Long? = null
)

