package com.loccitane.accelerateit.minipim.model

import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.Id

/**
@author Alexandre Masselot
@Copyright L'Occitane 2021
 */
@Entity
data class Segment(
    @Column(unique = true)
    var code: String,
    @Column(unique = true)
    var description: String,
    @Id @GeneratedValue
    var id: Long? = null
) {
}
