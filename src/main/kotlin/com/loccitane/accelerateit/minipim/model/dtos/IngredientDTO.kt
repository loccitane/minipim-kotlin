package com.loccitane.accelerateit.minipim.model.dtos

/**
@author Alexandre Masselot
@Copyright L'Occitane 2021
 */
data class IngredientDTO(
    val name: String,
    val families: List<String>
)
