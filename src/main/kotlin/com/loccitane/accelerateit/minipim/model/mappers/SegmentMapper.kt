package com.loccitane.accelerateit.minipim.model.mappers

import com.loccitane.accelerateit.minipim.model.Logistics
import com.loccitane.accelerateit.minipim.model.Segment
import com.loccitane.accelerateit.minipim.model.dtos.LogisticsDTO
import com.loccitane.accelerateit.minipim.model.dtos.SegmentDTO

/**
@author Alexandre Masselot
@Copyright L'Occitane 2021
 */
object SegmentMapper {
    fun fromDto(dto: SegmentDTO) =
        Segment(
            code = dto.code,
            description = dto.description
        )

    fun toDto(segment: Segment) =
        SegmentDTO(
            code = segment.code,
            description = segment.description
        )
}
