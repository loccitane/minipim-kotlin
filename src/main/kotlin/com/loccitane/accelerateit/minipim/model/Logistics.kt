package com.loccitane.accelerateit.minipim.model

import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.Id

/**
@author Alexandre Masselot
@Copyright L'Occitane 2021
 */
@Entity
data class Logistics(
    @Column
    var netWeight: Double,
    @Column
    var grossWeight: Double,
    @Column
    var height: Double,
    @Column
    var width: Double,
    @Column
    var length: Double,
    @Id @GeneratedValue
    var id: Long? = null
)
