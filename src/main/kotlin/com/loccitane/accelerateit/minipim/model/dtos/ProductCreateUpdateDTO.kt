package com.loccitane.accelerateit.minipim.model.dtos

import java.sql.Date

/**
@author Alexandre Masselot
@Copyright L'Occitane 2021
 */
data class ProductCreateUpdateDTO(
    val sku: String,
    val replaced_by: String?,
    val description: String,
    val segment: SegmentDTO,
    val mad: Date,
    val ingredients: List<String>?,
    val logistics: LogisticsDTO,
    val product_quantities: Map<String, Int>?
)
