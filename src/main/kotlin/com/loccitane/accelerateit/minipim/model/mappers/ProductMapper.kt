package com.loccitane.accelerateit.minipim.model.mappers

import com.loccitane.accelerateit.minipim.model.Ingredient
import com.loccitane.accelerateit.minipim.model.Product
import com.loccitane.accelerateit.minipim.model.dtos.KitToProductRelationshipDTO
import com.loccitane.accelerateit.minipim.model.dtos.ProductCreateUpdateDTO
import com.loccitane.accelerateit.minipim.model.dtos.ProductDTO
import com.loccitane.accelerateit.minipim.model.dtos.ProductSummaryDTO
import java.sql.Timestamp
import java.time.Instant

/**
@author Alexandre Masselot
@Copyright L'Occitane 2021
 */
object ProductMapper {
    fun fromDto(dto: ProductCreateUpdateDTO) =
        Product(
            sku = dto.sku,
            replacedBy = dto.replaced_by,
            description = dto.description,
            segment = SegmentMapper.fromDto(dto.segment),
            mad = dto.mad,
            ingredients = dto.ingredients?.map { Ingredient(name = it) }?.toMutableSet() ?: mutableSetOf(),
            logistics = LogisticsMapper.fromDto(dto.logistics),
            modifiedAt = Timestamp.from(Instant.now()),
        )

    fun toDto(product: Product): ProductDTO {
        val productQuantitites = product.productQuantities.map {
            KitToProductRelationshipDTO(
                it.quantity,
                ProductMapper.toDto(it.product)
            )
        }.let {
            if (it.isEmpty()) {
                null
            } else {
                it
            }
        }

        return ProductDTO(
            sku = product.sku,
            type = product.getProductType(),
            replaced_by = product.replacedBy,
            description = product.description,
            segment = SegmentMapper.toDto(product.segment),
            mad = product.mad,
            ingredients = product.ingredients.map { it.name }.toList().sortedBy { it },
            formula_contains = product.ingredients.flatMap { it.families }.map { it.name }.distinct().sorted(),
            logistics = LogisticsMapper.toDto(product.logistics),
            product_quantities = productQuantitites,
            modified_at = product.modifiedAt.toInstant(),
        )
    }


    fun toSummaryDto(product: Product): ProductSummaryDTO {
        val productsSkus = if (product.productQuantities.isEmpty()) {
            null
        } else {
            product.productQuantities.map { it.product.sku }.sorted()
        }
        return ProductSummaryDTO(
            type = product.getProductType(),
            sku = product.sku,
            description = product.description,
            products = productsSkus
        )
    }
}


