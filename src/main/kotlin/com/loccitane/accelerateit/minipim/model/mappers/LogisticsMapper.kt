package com.loccitane.accelerateit.minipim.model.mappers

import com.loccitane.accelerateit.minipim.model.Logistics
import com.loccitane.accelerateit.minipim.model.dtos.LogisticsDTO

/**
@author Alexandre Masselot
@Copyright L'Occitane 2021
 */
object LogisticsMapper {
    fun fromDto(dto: LogisticsDTO) =
        Logistics(
            netWeight = dto.netWeight,
            grossWeight = dto.grossWeight,
            height = dto.height,
            width = dto.width,
            length = dto.length
        )

    fun toDto(logistics: Logistics) =
        LogisticsDTO(
            netWeight = logistics.netWeight,
            grossWeight = logistics.grossWeight,
            height = logistics.height,
            width = logistics.width,
            length = logistics.length
        )
}
