package com.loccitane.accelerateit.minipim.model.mappers

import com.loccitane.accelerateit.minipim.model.Ingredient
import com.loccitane.accelerateit.minipim.model.IngredientFamily
import com.loccitane.accelerateit.minipim.model.dtos.IngredientDTO

/**
@author Alexandre Masselot
@Copyright L'Occitane 2021
 */

object IngredientMapper {
    fun fromDto(dto: IngredientDTO) =
        Ingredient(
            dto.name,
            dto.families.map { IngredientFamily(it) }.toMutableSet()
        )

    fun toDto(ingredient: Ingredient) =
        IngredientDTO(
            ingredient.name,
            ingredient.families.map { it.name }.sorted()
        )
}
