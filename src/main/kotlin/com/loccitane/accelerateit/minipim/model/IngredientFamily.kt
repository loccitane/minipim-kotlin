package com.loccitane.accelerateit.minipim.model

import javax.persistence.*


/**
@author Alexandre Masselot
@Copyright L'Occitane 2021
 */

@Entity
data class IngredientFamily(
    @Column(unique = true)
    var name: String,
    @Id @GeneratedValue
    var id: Long? = null
)

