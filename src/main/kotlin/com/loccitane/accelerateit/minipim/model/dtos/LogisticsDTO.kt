package com.loccitane.accelerateit.minipim.model.dtos

/**
@author Alexandre Masselot
@Copyright L'Occitane 2021
 */
data class LogisticsDTO(
    val netWeight: Double,
    val grossWeight: Double,
    val height: Double,
    val width: Double,
    val length: Double
)
