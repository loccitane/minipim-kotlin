package com.loccitane.accelerateit.minipim.model

import javax.persistence.*


/**
@author Alexandre Masselot
@Copyright L'Occitane 2021
 */

@Entity
data class Ingredient(
    @Column(unique = true)
    var name: String,
    @ManyToMany(fetch = FetchType.EAGER)
    var families: MutableSet<IngredientFamily> = mutableSetOf(),
    @Id @GeneratedValue
    var id: Long? = null
)
