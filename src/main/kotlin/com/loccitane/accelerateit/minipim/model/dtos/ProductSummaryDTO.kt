package com.loccitane.accelerateit.minipim.model.dtos

import com.fasterxml.jackson.annotation.JsonInclude
import com.loccitane.accelerateit.minipim.model.ProductType

/**
@author Alexandre Masselot
@Copyright L'Occitane 2021
 */

@JsonInclude(JsonInclude.Include.NON_NULL)
data class ProductSummaryDTO(
    val type: ProductType,
    val sku: String,
    val description: String,
    val products: List<String>?
)
