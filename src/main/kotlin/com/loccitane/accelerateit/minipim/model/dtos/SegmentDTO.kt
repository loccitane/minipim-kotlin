package com.loccitane.accelerateit.minipim.model.dtos

/**
@author Alexandre Masselot
@Copyright L'Occitane 2021
 */
data class SegmentDTO(
    val code: String,
    val description: String
)
