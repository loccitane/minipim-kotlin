package com.loccitane.accelerateit.minipim.model.dtos

/**
@author Alexandre Masselot
@Copyright L'Occitane 2021
 */
data class KitToProductRelationshipDTO(
    val quantity: Int,
    val product: ProductDTO
)
