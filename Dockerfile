FROM openjdk:17

COPY build/libs/minipim-*jar /app/minipim.jar

EXPOSE 8080

WORKDIR /app
CMD ["java", "-jar", "minipim.jar"]
