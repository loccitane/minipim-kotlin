#!/bin/bash
IFS=$'\n'       # make newlines the only separator
set -f   
set -e

for line in $( cat  $2); do
  echo $line | curl $1/ingredients --header 'Content-Type: application/json' -d @-
done
