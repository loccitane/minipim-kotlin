# Mini PIM
## Accelerate IT @ L'Occitane 

For the sake of the Accelerate IT course at L'Occitane, here is a small project.
It demonstrates some features of an API based Product Information system 

A Spring Boot powered API 

http://localhost:8080/swagger-ui/


## Testing

Junit test can be ran either through the IDE or viw ./gradlew test

### Postman

Import tests/postman file (collections & environments)

### Unit test

./gradlew test

## Docker

    version=$(./gradlew properties | grep '^version' | sed 's/version: //')
    ./gradlew clean build

    docker build --tag minipim:$version --tag minipim:latest --tag alexmass/minipim:$version --tag alexmass/minipim:latest .
    docker push alexmass/minipim:$version
    docker push alexmass/minipim:latest

## Import data

    cat  ~/Downloads/pim-products.json |
      jq -c '.results[] | 
        select(.productType == "PRODUCT") |
        {
            sku: .skuCode,
            description: .generalInformation.erpProductName,
            replaced_by: .generalInformation.replacedBy,
            segment:{
                code:.generalInformation.segment.code,
                description:.generalInformation.segment.value
            },
            mad: .generalInformation.madSeries,
            logistics:{
                netWeight:.logistics.netWeight,
                grossWeight: .logistics.grossWeight, 
                height:.logistics.height, 
                length:.logistics.length, 
                width: .logistics.width
            },
            ingredients:.scientificInformation.inciList
        }' > ~/tmp/minipim-products.rjson

    cat  ~/Downloads/pim-products.json |
      jq -c '.results[] |
        select(.productType == "KIT") |
        {
            sku: .skuCode,
            description: .generalInformation.erpProductName,
            replaced_by: .generalInformation.replacedBy,
            segment:{
                code:.generalInformation.segment.code,
                description:.generalInformation.segment.value
            },
            mad: .generalInformation.madSeries,
            logistics:{
                netWeight:.logistics.netWeight,
                grossWeight: .logistics.grossWeight,
                height:.logistics.height,
                length:.logistics.length,
                width: .logistics.width
            },
            ingredients:.scientificInformation.inciList, product_quantities: ((.productReferences.kitContains | select(length >0)) | reduce .[] as $i ({}; .[$i.productSummary.skuCode] = $i.attributes.quantity))
        }' > ~/tmp/minipim-kits.rjson

    MINIPIM_URL=http://localhost:8080
    MINIPIM_URL=https://minipim.azurewebsites.net

    scripts/import-ingredients.sh $MINIPIM_URL ~/tmp/minipim-ingredients.rjson
    scripts/import-products.sh $MINIPIM_URL ~/tmp/minipim-products.rjson
    scripts/import-products.sh $MINIPIM_URL ~/tmp/minipim-kits.rjson
